1. List the Books Authored by Marjorie Green:
The Busy Executive's Database Guide
You Can Combat Computer Stress!

2. List the Books Authored by Michael O' Leary.
Cooking with Computers



3. Write the author/s of "The Busy Executive's Database Guide"
Marjorie Green
Abraham Bennet


4. Identify the publisher of "But Is It User Friendly?"
Algodata Infosystems



5. List the books published by Algodata Infosystems.
Algodata Infosystems
The Busy Executive's Database Guide
Cooking with Computers
Straight Talk About Computers
But Is It User Friendly
Secrets of Silicon Valley
Net Etiquette